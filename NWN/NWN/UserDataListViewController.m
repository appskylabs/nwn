//
//  UserDataListViewController.m
//  NWN
//
//  Created by Taylor Korensky on 4/11/16.
//  Copyright © 2016 IOS Team. All rights reserved.
//

#import "UserDataListViewController.h"

@interface UserDataListViewController ()

@end

@implementation UserDataListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.dataPointsArray = [[NSMutableArray alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self getDataPoints];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) getDataPoints{
    
    [NetworkHelper getWithService:@"/api/datapoints/getuser" params:nil success:^(id response) {
        
        [self gotDataPoints: (NSMutableDictionary *)response];
        
    } failure:^(NSString *message) {
        
        [BFAlertControllerHelper showErrorAlertWithViewController:self message:message];
        
    }];
}

-(void) gotDataPoints: (NSMutableDictionary *)data {
    NSLog(@"User Datapoints: %@", data);
    
    NSMutableArray *tempArray = [[NSMutableArray alloc] initWithArray:(NSArray *)[data objectForKey:@"datapoints"]];
    for(int x = 0; x < tempArray.count; x++){
        
        NSMutableDictionary *temp = [tempArray objectAtIndex:x];
        [self.dataPointsArray addObject:temp];
        
    }

    [self.tableView reloadData];

}

- (IBAction)backButtonClicked:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark TableViewDelegate

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if( cell == nil ) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    NSMutableDictionary *tempDict = [self.dataPointsArray objectAtIndex:indexPath.row];
    NSString *dateString = [BFDateFormatHelper dateShort: [BFDateFormatHelper getLocalDateFromServerString: [tempDict objectForKey:@"created_at"]]];
    
    [cell setBackgroundColor:[UIColor clearColor]];

    UILabel *dateLabel = (UILabel *)[cell viewWithTag:1];
    [dateLabel setText: dateString];
     

    UILabel *latLabel = (UILabel *)[cell viewWithTag:2];
    [latLabel setText:[NSString stringWithFormat:@"Lat: %@",[tempDict objectForKey:@"gps_lat"]]];
    
    UILabel *longLabel = (UILabel *)[cell viewWithTag:3];
    [longLabel setText:[NSString stringWithFormat:@"Long: %@",[tempDict objectForKey:@"gps_long"]]];
    
    UILabel *resultLabel = (UILabel *)[cell viewWithTag:4];
    if([[tempDict objectForKey:@"result"] isEqualToString:@"Yes"]){
        
        [resultLabel setText:[NSString stringWithFormat:@"POSITIVE"]];
    }
    else if([[tempDict objectForKey:@"result"] isEqualToString:@"No"]){
        
        [resultLabel setText:[NSString stringWithFormat:@"NEGATIVE"]];
    }
    else if([[tempDict objectForKey:@"result"] isEqualToString:@"Inconclusive"]){
        
        [resultLabel setText:[NSString stringWithFormat:@"INCONCLUSIVE"]];
    }


    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataPointsArray.count;
}


@end
