//
//  DetermineResultViewController.h
//  NWN
//
//  Created by Taylor Korensky on 3/14/16.
//  Copyright © 2016 IOS Team. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddDataPointViewController.h"

@interface DetermineResultViewController : UIViewController
- (IBAction)addDataPoint:(id)sender;

@end
