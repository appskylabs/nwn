//
//  BFDateFormatHelper.h
//

#import <Foundation/Foundation.h>
#import "ISO8601.h"

@interface BFDateFormatHelper : NSObject
+(NSString *)dateShort:(NSDate*)date;
+(NSString *)dateServer:(NSDate*)date;
+(NSString *)dateServerLegacy:(NSDate *)date;
+(NSDate *)dateStringServerLegacy:(NSString*)string;
+(NSDateFormatter *)getDateFormatterWithFormat:(NSString *)format;
+(NSString *)dateShortWithTime:(NSDate*)date;
+(NSString *) getUTCStringFromDate: (NSDate *)date;

+(NSDate *) getLocalDateFromServerString: (NSString *) string;

+ (NSDate *)stampCurrentTimeOntoDate:(NSDate *)date;

+ (NSDate *)getDateFromString:(NSString *)iso8601String;

@end
