//
//  ViewController.m
//  NWN
//
//  Created by Taylor Korensky on 2/18/16.
//  Copyright © 2016 IOS Team. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)signUpClicked:(id)sender {
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:@"tak" forKey:@"first_name"];
    [dict setObject:@"korensky" forKey:@"last_name"];
    [dict setObject:@"bnall_master@hotmail.com" forKey:@"email"];
    [dict setObject:@"password" forKey:@"password"];
    [dict setObject:@"2147 S 49th Ave" forKey:@"address"];
    [dict setObject:@"Omaha" forKey:@"city"];
    [dict setObject:@"NE" forKey:@"state"];
    [dict setObject:@"68106" forKey:@"zip"];
    
    [NetworkHelper postWithService:@"/api/users/token" body:dict auth:nil success:^(id response) {
        
        NSLog(@"Response SUCCESS: %@", response);
        
    } failure:^(NSString *message) {
        
        NSLog(@"FAILED: %@", message);
    }];
}
@end
