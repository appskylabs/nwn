//
//  SignUpViewController.m
//  NWN
//
//  Created by Taylor Korensky on 3/1/16.
//  Copyright © 2016 IOS Team. All rights reserved.
//

#import "SignUpViewController.h"

@interface SignUpViewController ()

@end

@implementation SignUpViewController


-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    self.signInButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.signInButton.layer.borderWidth = 2.0f;
    
    self.signUpButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.signUpButton.layer.borderWidth = 2.0f;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)signUpClicked:(id)sender {
    
    if(self.emailTextField.text.length > 0 && self.firstNameTextField.text.length > 0 && self.lastNameTextField.text.length > 0 && self.passwordTextField.text.length > 0 && self.addressTextField.text.length > 0 && self.cityTextField.text.length > 0 && self.stateTextField.text.length > 0 && self.zipCodeTextField.text.length > 0){
        
        if(self.passwordTextField.text.length > 7 && self.verifyTextField.text.length > 7){
        if([self.passwordTextField.text isEqualToString:self.verifyTextField.text]){
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            [dict setObject: self.firstNameTextField.text forKey:@"first_name"];
            [dict setObject: self.lastNameTextField.text forKey:@"last_name"];
            [dict setObject: self.emailTextField.text forKey:@"email"];
            [dict setObject: self.passwordTextField.text forKey:@"password"];
             [dict setObject: self.verifyTextField.text forKey:@"password_verify"];
            [dict setObject: self.addressTextField.text forKey:@"address"];
            [dict setObject: self.cityTextField.text forKey:@"city"];
            [dict setObject: self.stateTextField.text forKey:@"state"];
            [dict setObject: self.zipCodeTextField.text forKey:@"zip"];
            
            [NetworkHelper postWithService:@"/api/users/add" body:dict auth:nil success:^(id response) {
                
                NSMutableDictionary *dict = [(NSMutableDictionary *)response valueForKey:@"data"];
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setObject:[dict objectForKey:@"token"] forKey:@"token"];
                [defaults synchronize];
                self.firstNameTextField.text = @"";
                self.lastNameTextField.text = @"";
                self.emailTextField.text = @"";
                self.passwordTextField.text = @"";
                self.addressTextField.text = @"";
                self.cityTextField.text = @"";
                self.stateTextField.text = @"";
                self.zipCodeTextField.text = @"";
                self.verifyTextField.text = @"";
                
                NSLog(@"Sign up Response SUCCESS: %@", response);
            
                [self performSelector:@selector(gotToHomePage) withObject:nil afterDelay:0.5];
                
            } failure:^(NSString *message) {
                
                NSLog(@"FAILED: %@", message);
                [BFAlertControllerHelper showErrorAlertWithViewController:self message:message];
            }];
            
        }
        else{
            [BFAlertControllerHelper showAlertWithViewController:self title:@"Missing Info" message:@"Passwords do not match"];
            
        }
        }
        else{
           [BFAlertControllerHelper showAlertWithViewController:self title:@"Missing Info" message:@"Password must be at least 8 characters"];
        }
    }
    else{
        
        [BFAlertControllerHelper showAlertWithViewController:self title:@"Missing Info" message:@"Make sure all fields are filled out."];
        
    }
}


-(void) gotToHomePage{
    
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
     MainViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"mainView"];
    [self presentViewController:viewController animated:YES completion:nil];
}


- (IBAction)nextOnKeyboard:(id)sender {
    
    if(sender == self.firstNameTextField){
        [self.firstNameTextField resignFirstResponder];
        [self.lastNameTextField becomeFirstResponder];
    }
    else if(sender == self.lastNameTextField){
        [self.lastNameTextField resignFirstResponder];
        [self.emailTextField becomeFirstResponder];
    }
    else if(sender == self.emailTextField){
        [self.emailTextField resignFirstResponder];
        [self.addressTextField becomeFirstResponder];
        
    }else if(sender == self.addressTextField){
        [self.addressTextField resignFirstResponder];
        [self.cityTextField becomeFirstResponder];
    }
    else if(sender == self.cityTextField){
        [self.cityTextField resignFirstResponder];
        [self.stateTextField becomeFirstResponder];
    }
    else if(sender == self.stateTextField){
        [self.stateTextField resignFirstResponder];
        [self.zipCodeTextField becomeFirstResponder];
    }
    else if(sender == self.zipCodeTextField){
        [self.zipCodeTextField resignFirstResponder];
        [self.passwordTextField becomeFirstResponder];
    }
    else if(sender == self.passwordTextField){
        [self.passwordTextField resignFirstResponder];
        [self.verifyTextField becomeFirstResponder];
    }
    else{
        [self.verifyTextField resignFirstResponder];
        [self signUpClicked:nil];
    }
}
@end
