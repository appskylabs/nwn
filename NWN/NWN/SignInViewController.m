//
//  SignInViewController.m
//  NWN
//
//  Created by Taylor Korensky on 3/1/16.
//  Copyright © 2016 IOS Team. All rights reserved.
//

#import "SignInViewController.h"

@interface SignInViewController ()

@end

@implementation SignInViewController

-(void) viewWillAppear:(BOOL)animated   {
    
    [super viewWillAppear:animated];
    self.signInButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.signInButton.layer.borderWidth = 2.0f;
    
    self.createAccountButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.createAccountButton.layer.borderWidth = 2.0f;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void) viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"token"] == nil || [[defaults objectForKey:@"token"] isEqual:[NSNull null]] || [[defaults objectForKey:@"token"] length] == 0){
        
    }
    else{
        [self gotToHomePage];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)signInClicked:(id)sender {
    
    if(self.emailTextField.text.length > 0 && self.passwordTextField.text.length > 0){
        
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        [dict setObject: self.emailTextField.text forKey:@"email"];
        [dict setObject: self.passwordTextField.text forKey:@"password"];
        
        [NetworkHelper postWithService:@"/api/users/token" body:dict auth:nil success:^(id response) {
            
            NSLog(@"Sign in Response SUCCESS: %@", response);
            NSMutableDictionary *dict = [(NSMutableDictionary *)response valueForKey:@"data"];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:[dict objectForKey:@"token"] forKey:@"token"];
            [defaults synchronize];
            
            [self gotToHomePage];
            
        } failure:^(NSString *message) {
            
            NSLog(@"FAILED: %@", message);
            [BFAlertControllerHelper showErrorAlertWithViewController:self message:@"Invalid login information."];
        }];
    }
    else{
        
        [BFAlertControllerHelper showAlertWithViewController:self title:@"Missing Info" message:@"Make sure all fields are filled out."];
        
    }
}

-(void) gotToHomePage{
    
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    MainViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"mainView"];
    [self presentViewController:viewController animated:YES completion:nil];
}


- (IBAction)nextOnKeyboard:(id)sender {
    
    if(sender == self.emailTextField){
        [self.emailTextField resignFirstResponder];
        [self.passwordTextField becomeFirstResponder];
    }
    else{
        [self.passwordTextField resignFirstResponder];
    }
}
@end
