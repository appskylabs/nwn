//
//  ViewController.h
//  NWN
//
//  Created by Taylor Korensky on 2/18/16.
//  Copyright © 2016 IOS Team. All rights reserved.
//

#import "NetworkHelper.h"
#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *signUpButton;
- (IBAction)signUpClicked:(id)sender;

@end

