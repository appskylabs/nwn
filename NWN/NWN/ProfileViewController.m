//
//  ProfileViewController.m
//  NWN
//
//  Created by Taylor Korensky on 3/31/16.
//  Copyright © 2016 IOS Team. All rights reserved.
//

#import "ProfileViewController.h"

@interface ProfileViewController ()

@end

@implementation ProfileViewController

-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    self.saveButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.saveButton.layer.borderWidth = 2.0f;
    [self getUser];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) getUser{
    
    [NetworkHelper getWithService:@"/api/users/view" params:nil success:^(id response) {
        NSLog(@"Profile: %@", response);
        NSMutableDictionary *dict = [(NSMutableDictionary *)response objectForKey:@"user"];
        self.firstNameTextfield.text = [dict objectForKey:@"first_name"];
        self.lastNameTextField.text = [dict objectForKey:@"last_name"];
        self.emailTextField.text = [dict objectForKey:@"email"];
        self.addressTextField.text = [dict objectForKey:@"address"];
        self.cityTextField.text = [dict objectForKey:@"city"];
        self.stateTextField.text = [dict objectForKey:@"state"];
        self.zipcodeTextField.text = [NSString stringWithFormat:@"%d", [[dict objectForKey:@"zip"] intValue]];
        
    } failure:^(NSString *message) {
        
        [BFAlertControllerHelper showErrorAlertWithViewController:self message:message];

    }];
}

- (IBAction)saveButtonClicked:(id)sender {
    
    
}
@end
