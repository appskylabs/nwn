//
//  SignInViewController.h
//  NWN
//
//  Created by Taylor Korensky on 3/1/16.
//  Copyright © 2016 IOS Team. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NetworkHelper.h"
#import "BFAlertControllerHelper.h"
#import "MainViewController.h"


@interface SignInViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
- (IBAction)signInClicked:(id)sender;
- (IBAction)nextOnKeyboard:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *signInButton;
@property (strong, nonatomic) IBOutlet UIButton *createAccountButton;

@end
