//
//  TimerViewController.m
//  NWN
//
//  Created by Taylor Korensky on 3/16/16.
//  Copyright © 2016 IOS Team. All rights reserved.
//

#import "TimerViewController.h"

@interface TimerViewController ()

@end

@implementation TimerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.nextStep setHidden:YES];
    MZTimerLabel *timer = [[MZTimerLabel alloc] initWithLabel:self.timerLabel andTimerType:MZTimerLabelTypeTimer];
    [timer setTimeFormat:@"mm:ss"];
    [timer setCountDownTime:600];
    [timer startWithEndingBlock:^(NSTimeInterval countTime) {
        //oh my gosh, it's awesome!!
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.nextStep setHidden:NO];
        });
        [BFAlertControllerHelper showAlertWithViewController:self title:@"Test Complete" message:@"You can now pull move onto the next step!"];
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
