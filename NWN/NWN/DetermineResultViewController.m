//
//  DetermineResultViewController.m
//  NWN
//
//  Created by Taylor Korensky on 3/14/16.
//  Copyright © 2016 IOS Team. All rights reserved.
//

#import "DetermineResultViewController.h"

@interface DetermineResultViewController ()

@end

@implementation DetermineResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)addDataPoint:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AddDataPointViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"addDataPoint"];
    [self presentViewController:viewController animated:YES completion:nil];
}
@end
