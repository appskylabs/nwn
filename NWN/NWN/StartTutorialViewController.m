//
//  StartTutorialViewController.m
//  NWN
//
//  Created by Taylor Korensky on 3/31/16.
//  Copyright © 2016 IOS Team. All rights reserved.
//

#import "StartTutorialViewController.h"

@interface StartTutorialViewController ()

@end

@implementation StartTutorialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
