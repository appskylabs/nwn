//
//  MainViewController.h
//  NWN
//
//  Created by Taylor Korensky on 3/1/16.
//  Copyright © 2016 IOS Team. All rights reserved.
//
#import "SignInViewController.h"
#import <UIKit/UIKit.h>
#import "AddDataPointViewController.h"
#import <MessageUI/MessageUI.h>
#import "NetworkHelper.h"
#import "StartTutorialViewController.h"
#import "UserDataListViewController.h"

@interface MainViewController : UIViewController <MFMailComposeViewControllerDelegate, UITableViewDelegate, UITableViewDataSource>

- (IBAction)logout:(id)sender;
- (IBAction)addDataPoint:(id)sender;
- (IBAction)contactNWM:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *projectsArray;
- (IBAction)mySavedDataClicked:(id)sender;

@end
