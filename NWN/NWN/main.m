//
//  main.m
//  NWN
//
//  Created by Taylor Korensky on 2/18/16.
//  Copyright © 2016 IOS Team. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
