//
//  BFDateFormatHelper.m


#import "BFDateFormatHelper.h"

@implementation BFDateFormatHelper

+(NSString *)dateShort:(NSDate*)date{
    return [[self getDateFormatterWithFormat:@"MMM d, yyy"] stringFromDate:date];
}
+(NSString *)dateShortWithTime:(NSDate*)date{
    return [NSString stringWithFormat:@"%@ at %@",[[self getDateFormatterWithFormat:@"MMM d"] stringFromDate:date], [[self getDateFormatterWithFormat:@"hh:mm a"] stringFromDate:date]];
}
+(NSString *)dateServer:(NSDate*)date{
    return [[self getDateFormatterWithFormat:@"yyyy-MM-dd HH:mm:ss"] stringFromDate:date];
}
+(NSString *)dateServerLegacy:(NSDate *)date{
    return [[self getDateFormatterWithFormat:@"MM/dd/yyyy"] stringFromDate:date];
}
+(NSDate *)dateStringServerLegacy:(NSString*)string{
    return [[self getDateFormatterWithFormat:@"dd-MM-yyyy"] dateFromString:string];
}
+(NSDateFormatter *)getDateFormatterWithFormat:(NSString *)format{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *enUSPOSIXLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:enUSPOSIXLocale];
    [dateFormatter setDateFormat:format];
    return dateFormatter;
}

+ (NSDate *)getDateFromString:(NSString *)iso8601String {
    
    return [NSDate dateWithISO8601String:iso8601String];
}

#pragma mark - sets current time on date; returns result

+ (NSDate *)stampCurrentTimeOntoDate:(NSDate *)date {
    
    NSString *currentDateString = date.description;
    NSArray *frankenCurrentDateParts = [currentDateString componentsSeparatedByString:@" "];
    
    NSDate *today = [NSDate date];
    NSString *todayString = today.description;
    NSArray *frankenTodayParts = [todayString componentsSeparatedByString: @" "];
    
    NSArray *frankenDateParts =  @[frankenCurrentDateParts[0],frankenTodayParts[1],frankenCurrentDateParts[2]];
    NSString *frankenDatePartsString = [frankenDateParts componentsJoinedByString:@" "];
    
    return [self getDateFromString:frankenDatePartsString];
}

+(NSDate *) getLocalDateFromServerString: (NSString *) string{
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    
    NSDate *stringDate = [BFDateFormatHelper getDateFromString:string];
    
    NSString *timestamp = [dateFormatter stringFromDate:stringDate];
    
    return [dateFormatter dateFromString:timestamp];
    
}

+(NSString *) getUTCStringFromDate: (NSDate *)date{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone: [NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    return  [dateFormatter stringFromDate:date];
}


@end
