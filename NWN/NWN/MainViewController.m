//
//  MainViewController.m
//  NWN
//
//  Created by Taylor Korensky on 3/1/16.
//  Copyright © 2016 IOS Team. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController

-(void) viewWillAppear:(BOOL)animated{
    
    [self.tableView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.projectsArray = [[NSMutableArray alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self getProjects];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) getProjects {
    
    [NetworkHelper getWithService:@"/api/projects/active" params:nil success:^(id response) {
        
        [self gotProjects: (NSMutableDictionary *)response];
        
    } failure:^(NSString *message) {
        
        [BFAlertControllerHelper showErrorAlertWithViewController:self message:message];
        
    }];
}

-(void) gotProjects: (NSMutableDictionary *)array{
    
    NSMutableArray *tempArray = [[NSMutableArray alloc] initWithArray:(NSArray *)[array objectForKey:@"projects"]];
    for(int x = 0; x < tempArray.count; x++){
        
        NSMutableDictionary *temp = [tempArray objectAtIndex:x];
        [self.projectsArray addObject:temp];
        
    }
     NSLog(@"Projects: %@", self.projectsArray);
    [self.tableView reloadData];
    
}


- (IBAction)logout:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SignInViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"signInView"];
    [self presentViewController:viewController animated:YES completion:nil];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:nil forKey:@"token"];
    [defaults synchronize];
}

- (IBAction)addDataPoint:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AddDataPointViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"addDataPoint"];
    [self presentViewController:viewController animated:YES completion:nil];
}

- (IBAction)contactNWM:(id)sender {
    
    if ([MFMailComposeViewController canSendMail])
    {
        
        MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
        
        mail.mailComposeDelegate=self;
        
        [mail setSubject:@"Contact NWN"];
        [mail setToRecipients:@[@"unonwn@unomaha.edu"]];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:mail animated:YES completion:NULL];
        });
    }
    else
    {
        [BFAlertControllerHelper showErrorAlertWithViewController:self message:@"Please setup an email on your device."];
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if( cell == nil ) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    UILabel *label = (UILabel *)[cell viewWithTag:1];
    NSMutableDictionary *tempDict = [self.projectsArray objectAtIndex:indexPath.row];
    [cell setBackgroundColor:[UIColor clearColor]];
    [label setText:[tempDict objectForKey:@"name"]];

    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.projectsArray.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableDictionary *tempDict = [self.projectsArray objectAtIndex:indexPath.row];
    
    [[NSUserDefaults standardUserDefaults] setObject:[tempDict objectForKey:@"projectid"] forKey:@"projectid"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSMutableArray *alertButtonTitles;

    alertButtonTitles = [[NSMutableArray alloc] initWithObjects:@"Start Tutorial Wizard", @"Add Data Point", nil];


    NSMutableArray *alertActionArray = [[NSMutableArray alloc] init];
    
    
    [alertActionArray addObject:^(UIAlertAction *action) {
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        StartTutorialViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"startTutorialView"];
        [self presentViewController:viewController animated:YES completion:nil];
    }];
    
    [alertActionArray addObject:^(UIAlertAction *action) {
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        AddDataPointViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"addDataPoint"];
        [self presentViewController:viewController animated:YES completion:nil];

    }];
        
        
    [BFAlertControllerHelper showMultiButtonAlertWithViewController:self title:@"Select Option" message:@"Please select one of the options below to begin."positiveButtonTitles: alertButtonTitles positiveHandlers:alertActionArray withCancelButtonTitle:@"Cancel"];
    
}
- (IBAction)mySavedDataClicked:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"UserDataListStoryboard" bundle:nil];
    UserDataListViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"userDataView"];
    [self presentViewController:viewController animated:YES completion:nil];
}
@end
