//
//  UserDataListViewController.h
//  NWN
//
//  Created by Taylor Korensky on 4/11/16.
//  Copyright © 2016 IOS Team. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BFAlertControllerHelper.h"
#import "NetworkHelper.h"
#import "BFDateFormatHelper.h"



@interface UserDataListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)backButtonClicked:(id)sender;
@property (strong, nonatomic) NSMutableArray *dataPointsArray;
@end
