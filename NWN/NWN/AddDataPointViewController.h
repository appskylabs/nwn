//
//  AddDataPointViewController.h
//  NWN
//
//  Created by Taylor Korensky on 3/9/16.
//  Copyright © 2016 IOS Team. All rights reserved.
//

#import "NetworkHelper.h"
#import "BFAlertControllerHelper.h"
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "MainViewController.h"
#import "SBFlatDatePicker.h"
#import "SBFlatDatePickerDelegate.h"
#import "BFDateFormatHelper.h"


@interface AddDataPointViewController : UIViewController <UITextViewDelegate, CLLocationManagerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, SBFLatDatePickerDelegate>
- (IBAction)backButtonClicked:(id)sender;
- (IBAction)saveDataPointClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *latitudeTextField;
@property (strong, nonatomic) IBOutlet UITextField *longitudeTextField;
@property (strong, nonatomic) IBOutlet UITextView *commentsView;
@property (strong, nonatomic) IBOutlet UISegmentedControl *resultSegmentController;
- (IBAction)nextOnKeyboard:(id)sender;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) UIImagePickerController *picker;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
- (IBAction)addImage:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *saveButton;
@property (strong, nonatomic) IBOutlet UIButton *addTestButton;

@property (strong, nonatomic) IBOutlet UIButton *dateButton;
- (IBAction)showDatePicker:(id)sender;
@property (strong, nonatomic) NSDate *selectedDate;
@end
