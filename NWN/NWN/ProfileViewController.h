//
//  ProfileViewController.h
//  NWN
//
//  Created by Taylor Korensky on 3/31/16.
//  Copyright © 2016 IOS Team. All rights reserved.
//

#import "NetworkHelper.h"
#import "BFAlertControllerHelper.h"
#import <UIKit/UIKit.h>

@interface ProfileViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *firstNameTextfield;
@property (strong, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UITextField *addressTextField;
@property (strong, nonatomic) IBOutlet UITextField *cityTextField;
@property (strong, nonatomic) IBOutlet UITextField *stateTextField;
@property (strong, nonatomic) IBOutlet UITextField *zipcodeTextField;
@property (strong, nonatomic) IBOutlet UIButton *saveButton;
- (IBAction)saveButtonClicked:(id)sender;


@end
