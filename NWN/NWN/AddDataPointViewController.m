//
//  AddDataPointViewController.m
//  NWN
//
//  Created by Taylor Korensky on 3/9/16.
//  Copyright © 2016 IOS Team. All rights reserved.
//

#import "AddDataPointViewController.h"

@interface AddDataPointViewController ()

@end

@implementation AddDataPointViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.selectedDate = [NSDate date];
    [self.dateButton setTitle:[BFDateFormatHelper dateServerLegacy:self.selectedDate] forState:UIControlStateNormal];
    
    self.commentsView.delegate = self;
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    [self.locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    [self.locationManager setDistanceFilter:kCLDistanceFilterNone];
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) { [self.locationManager requestWhenInUseAuthorization]; }
    [self.locationManager startUpdatingLocation];
    
    self.saveButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.saveButton.layer.borderWidth = 2.0f;
    
    self.addTestButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.addTestButton.layer.borderWidth = 2.0f;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backButtonClicked:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)saveDataPointClicked:(id)sender {
    
    if(self.latitudeTextField.text.length > 0 && self.longitudeTextField.text.length > 0){
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    [dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"projectid"] forKey:@"projectid"];
    if(self.resultSegmentController.selectedSegmentIndex == 0){
        [dict setObject:@"Yes" forKey:@"result"];
    }
    else if(self.resultSegmentController.selectedSegmentIndex == 1){
        [dict setObject:@"No" forKey:@"result"];
    }
    else{
        [dict setObject:@"Inconclusive" forKey:@"result"];
    }
    [dict setObject:self.commentsView.text forKey:@"comments"];
    [dict setObject: self.latitudeTextField.text forKey:@"gps_lat"];
    [dict setObject: self.longitudeTextField.text forKey:@"gps_long"];
    [dict setObject: [BFDateFormatHelper getUTCStringFromDate:self.selectedDate] forKey:@"created_at"];
    
    NSMutableArray *alertButtonTitles;
    
    alertButtonTitles = [[NSMutableArray alloc] initWithObjects:@"Test Again", @"Go to Home", nil];
    
    NSMutableArray *alertActionArray = [[NSMutableArray alloc] init];
    
    [alertActionArray addObject:^(UIAlertAction *action) {
        
        [self.commentsView setText:@"Comments here..."];
        [self.latitudeTextField setText:@""];
        [self.longitudeTextField setText:@""];
        [self.imageView setImage:nil];
        [self.locationManager startUpdatingLocation];
        
    }];
    
    [alertActionArray addObject:^(UIAlertAction *action) {
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        MainViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"mainView"];
        [self presentViewController:viewController animated:YES completion:nil];
        
    }];
    
    [BFAlertControllerHelper showMultiButtonAlertWithViewController:self title:@"Success" message:@"You added a data point successfully!" positiveButtonTitles:alertButtonTitles positiveHandlers:alertActionArray withCancelButtonTitle:@"Dismiss"];
    
    
    [NetworkHelper postWithService:@"/api/datapoints/add" body:dict auth:[NSMutableDictionary new] success:^(id response) {
        
        NSLog(@"Response SUCCESS: %@", response);
        
    } failure:^(NSString *message) {
        
        NSLog(@"FAILED: %@", message);
        [BFAlertControllerHelper showErrorAlertWithViewController:self message:message];
    }];
    }
    else{
        
        [BFAlertControllerHelper showErrorAlertWithViewController:self message:@"Make sure latitude and longitude fields are filled out."];
    }
}

- (IBAction)nextOnKeyboard:(id)sender {
    
    if(sender == _latitudeTextField){
        [self.latitudeTextField resignFirstResponder];
        [self.longitudeTextField becomeFirstResponder];
    }
    else if(sender == self.longitudeTextField){
        [self.longitudeTextField resignFirstResponder];
        [self.commentsView becomeFirstResponder];
    }
}


-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    //  NSLog(@"Location Array: %@", locations);
    if(locations.count > 0){
        CLLocation *location = [locations firstObject];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:[NSString stringWithFormat:@"%f",location.coordinate.latitude] forKey: @"latitude"];
        [defaults setObject:[NSString stringWithFormat:@"%f",location.coordinate.longitude] forKey:@"longitude"];
        self.latitudeTextField.text = [NSString stringWithFormat:@"%f",location.coordinate.latitude];
        self.longitudeTextField.text = [NSString stringWithFormat:@"%f",location.coordinate.longitude];
    }
}

-(void) locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    NSLog(@"Authorization: %d", status);
    if(status == 2){
        [self.locationManager requestWhenInUseAuthorization];
    }
    
}

#pragma mark - When finish shoot

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image=[info objectForKey:UIImagePickerControllerOriginalImage];
    [self.imageView setImage:image];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)addImage:(id)sender {
    
    self.picker =[[UIImagePickerController alloc]init];
    self.picker.sourceType=UIImagePickerControllerSourceTypeCamera;
    self.picker.delegate = self;
    [self presentViewController:self.picker animated:YES completion:nil];
    
}
- (IBAction)showDatePicker:(id)sender {
    
    UIViewController *pickerViewController = [[UIViewController alloc] init];
    
    //Init the datePicker view and set self as delegate
    SBFlatDatePicker *datePicker = [[SBFlatDatePicker alloc] initWithFrame:self.view.bounds];
    [datePicker setDelegate:self];
    
    //OPTIONAL: Choose the background color
    [datePicker setBackgroundColor:[UIColor whiteColor]];
    
    //OPTIONAL - Choose Date Range (0 starts At Today. Non continous sets acceptable (use some enumartion for [indexSet addIndex:yourIndex];
    datePicker.dayRange = [NSMutableIndexSet indexSetWithIndexesInRange:NSMakeRange(0, 365)];
    
    
    //OPTIONAL - Choose Minute  Non continous sets acceptable (use some enumartion for [indexSet addIndex:yourIndex];
    datePicker.minuterange = [NSMutableIndexSet indexSet];
    [datePicker.minuterange addIndex:0];
    [datePicker.minuterange addIndex:15];
    [datePicker.minuterange addIndex:30];
    [datePicker.minuterange addIndex:45];
    
    //Customize date format
    datePicker.dayFormat = @"EEE MMM dd";
    
    //Set the data picker as view of the new view controller
    [pickerViewController setView:datePicker];
    
    //Present the view controller
    [self presentViewController:pickerViewController animated:YES completion:nil];
    
}

-(void)flatDatePicker:(SBFlatDatePicker *)datePicker saveDate:(NSDate *)date{
    NSLog(@"%@",[date description]);
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.dateButton setTitle:[BFDateFormatHelper dateServerLegacy:date] forState:UIControlStateNormal];
    self.selectedDate = date;
    
}

-(void) textViewDidBeginEditing:(UITextView *)textView{
    
    if([self.commentsView.text isEqualToString:@"Comments here..."]){
        [self.commentsView setText:@""];
    }
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    
    if(self.commentsView.text.length == 0){
        [self.commentsView setText:@"Comments here..."];
    }
}


@end
