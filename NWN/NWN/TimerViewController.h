//
//  TimerViewController.h
//  NWN
//
//  Created by Taylor Korensky on 3/16/16.
//  Copyright © 2016 IOS Team. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MZTimerLabel.h"
#import "BFAlertControllerHelper.h"

@interface TimerViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *timerLabel;
@property (strong, nonatomic) IBOutlet UIButton *nextStep;

@end
