//
//  SignUpViewController.h
//  NWN
//
//  Created by Taylor Korensky on 3/1/16.
//  Copyright © 2016 IOS Team. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NetworkHelper.h"
#import "BFAlertControllerHelper.h"
#import "SignInViewController.h"
#import "MainViewController.h"

@interface SignUpViewController : UIViewController
- (IBAction)signUpClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (strong, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UITextField *addressTextField;
@property (strong, nonatomic) IBOutlet UITextField *cityTextField;
@property (strong, nonatomic) IBOutlet UITextField *stateTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UITextField *verifyTextField;
- (IBAction)nextOnKeyboard:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *zipCodeTextField;
@property (strong, nonatomic) IBOutlet UIButton *signUpButton;
@property (strong, nonatomic) IBOutlet UIButton *signInButton;

@end
